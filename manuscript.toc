\babel@toc {english}{}\relax 
\contentsline {chapter}{\numberline {1}Topological concepts in physics}{7}{chapter.1}%
\babel@toc {french}{}\relax 
\babel@toc {english}{}\relax 
\contentsline {section}{\numberline {1.1}Topology in a nutshell}{10}{section.1.1}%
\contentsline {subsection}{\numberline {1.1.1}Counting holes}{10}{subsection.1.1.1}%
\contentsline {subsection}{\numberline {1.1.2}Curvature: from local to global}{12}{subsection.1.1.2}%
\contentsline {subsection}{\numberline {1.1.3}Link with physics}{13}{subsection.1.1.3}%
\contentsline {section}{\numberline {1.2}Topology: from quantum Hall effect to classical waves}{14}{section.1.2}%
\contentsline {subsection}{\numberline {1.2.1}Quantum Hall effect}{14}{subsection.1.2.1}%
\contentsline {subsection}{\numberline {1.2.2}Mechanical waves}{15}{subsection.1.2.2}%
\contentsline {subsection}{\numberline {1.2.3}Acoustic waves}{15}{subsection.1.2.3}%
\contentsline {subsection}{\numberline {1.2.4}Equatorial waves}{17}{subsection.1.2.4}%
\contentsline {subsubsection}{\numberline {1.2.4.1}Aharonov-Bohm effect}{17}{subsubsection.1.2.4.1}%
\contentsline {subsubsection}{\numberline {1.2.4.2}Coriolis force}{18}{subsubsection.1.2.4.2}%
\contentsline {subsubsection}{\numberline {1.2.4.3}Kelvin waves}{19}{subsubsection.1.2.4.3}%
\contentsline {section}{\numberline {1.3}Topology in photonics}{19}{section.1.3}%
\contentsline {subsection}{\numberline {1.3.1}Genesis of photonic topology}{20}{subsection.1.3.1}%
\contentsline {subsection}{\numberline {1.3.2}The surprising topological Anderson insulator}{23}{subsection.1.3.2}%
\contentsline {subsection}{\numberline {1.3.3}Topological photonic fibers}{24}{subsection.1.3.3}%
\contentsline {subsection}{\numberline {1.3.4}Topological laser}{24}{subsection.1.3.4}%
\contentsline {subsection}{\numberline {1.3.5}{\color {red}Topology with ultracold atoms}}{25}{subsection.1.3.5}%
\contentsline {section}{\numberline {1.4}Summary}{25}{section.1.4}%
\contentsline {chapter}{\numberline {2}Tools and models to study topological phases}{27}{chapter.2}%
\babel@toc {english}{}\relax 
\contentsline {section}{\numberline {2.1}Graphene}{29}{section.2.1}%
\contentsline {subsection}{\numberline {2.1.1}Graphene's Brillouin zone}{29}{subsection.2.1.1}%
\contentsline {subsection}{\numberline {2.1.2}Graphene nanoribbon}{31}{subsection.2.1.2}%
\contentsline {section}{\numberline {2.2}Haldane model}{33}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}Infinite systems}{33}{subsection.2.2.1}%
\contentsline {subsection}{\numberline {2.2.2}Nanoribbon}{35}{subsection.2.2.2}%
\contentsline {subsection}{\numberline {2.2.3}Chern number}{36}{subsection.2.2.3}%
\contentsline {subsection}{\numberline {2.2.4}Haldane model on a lattice of finite size}{38}{subsection.2.2.4}%
\contentsline {subsection}{\numberline {2.2.5}Bott index}{39}{subsection.2.2.5}%
\contentsline {subsubsection}{\numberline {2.2.5.1}Bott index of two unitary matrices}{40}{subsubsection.2.2.5.1}%
\contentsline {subsubsection}{\numberline {2.2.5.2}Bott index of two invertible matrices}{41}{subsubsection.2.2.5.2}%
\contentsline {subsubsection}{\numberline {2.2.5.3}Vanishing of Bott index}{41}{subsubsection.2.2.5.3}%
\contentsline {subsubsection}{\numberline {2.2.5.4}Bott index for physicists}{42}{subsubsection.2.2.5.4}%
\contentsline {subsubsection}{\numberline {2.2.5.5}Bott index in Haldane model}{43}{subsubsection.2.2.5.5}%
\contentsline {section}{\numberline {2.3}Kane-Mele model}{44}{section.2.3}%
\contentsline {subsection}{\numberline {2.3.1}Band structure}{44}{subsection.2.3.1}%
\contentsline {subsection}{\numberline {2.3.2}Spin Chern number}{45}{subsection.2.3.2}%
\contentsline {subsection}{\numberline {2.3.3}Spin Bott index}{45}{subsection.2.3.3}%
\contentsline {chapter}{\numberline {3}Two-level atoms in the electromagnetic vacuum}{49}{chapter.3}%
\contentsline {section}{\numberline {3.1}An atom in free space}{51}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}Two-level model and dipolar approximation}{51}{subsection.3.1.1}%
\contentsline {subsection}{\numberline {3.1.2}Markovian approximation}{52}{subsection.3.1.2}%
\contentsline {subsection}{\numberline {3.1.3}The rotating wave approximation}{54}{subsection.3.1.3}%
\contentsline {section}{\numberline {3.2}$N$ atoms in free space}{55}{section.3.2}%
\contentsline {subsection}{\numberline {3.2.1}Maxwell equations}{55}{subsection.3.2.1}%
\contentsline {subsection}{\numberline {3.2.2}Wave equation in Fourier space}{55}{subsection.3.2.2}%
\contentsline {subsection}{\numberline {3.2.3}Green propagator}{56}{subsection.3.2.3}%
\contentsline {subsection}{\numberline {3.2.4}Effective Hamiltonian for $N$ atoms}{56}{subsection.3.2.4}%
\contentsline {section}{\numberline {3.3}Atom in the Fabry-P\'{e}rot cavity}{57}{section.3.3}%
\contentsline {subsection}{\numberline {3.3.1}Green's function in a Fabry-Pérot cavity}{58}{subsection.3.3.1}%
\contentsline {subsection}{\numberline {3.3.2}Frequency shift and lifetime}{58}{subsection.3.3.2}%
\contentsline {chapter}{\numberline {4}Topological properties of a honeycomb lattice of atoms}{61}{chapter.4}%
\contentsline {section}{\numberline {4.1}Honeycomb atomic lattice in the free space}{63}{section.4.1}%
\contentsline {subsection}{\numberline {4.1.1}Band diagram of a honeycomb lattice in the free space}{63}{subsection.4.1.1}%
\contentsline {subsection}{\numberline {4.1.2}Summations in momentum space}{65}{subsection.4.1.2}%
\contentsline {subsection}{\numberline {4.1.3}Width of the band gap}{67}{subsection.4.1.3}%
\contentsline {subsection}{\numberline {4.1.4}Topological properties of the band structure}{68}{subsection.4.1.4}%
\contentsline {section}{\numberline {4.2}Honeycomb atomic lattice in a Fabry-P\'{e}rot cavity}{72}{section.4.2}%
\contentsline {subsection}{\numberline {4.2.1}Band diagram of a honeycomb lattice in a Fabry-P\'{e}rot cavity}{72}{subsection.4.2.1}%
\contentsline {subsection}{\numberline {4.2.2}Topological properties of the band structure}{74}{subsection.4.2.2}%
\contentsline {section}{\numberline {4.3}Conclusion}{75}{section.4.3}%
\contentsline {chapter}{\numberline {5}Toward a Photonic Topological Anderson Insulator}{79}{chapter.5}%
\contentsline {section}{\numberline {5.1}A two-dimensional honeycomb atomic lattice of finite size}{81}{section.5.1}%
\contentsline {section}{\numberline {5.2}Density of states and band gaps}{81}{section.5.2}%
\contentsline {section}{\numberline {5.3}Computing the Bott index: a topological marker}{82}{section.5.3}%
\contentsline {section}{\numberline {5.4}Introducing disorder }{84}{section.5.4}%
\contentsline {subsection}{\numberline {5.4.1}Disorder as a topological killer}{85}{subsection.5.4.1}%
\contentsline {subsection}{\numberline {5.4.2}The emergence of a topological Anderson insulator}{86}{subsection.5.4.2}%
\contentsline {chapter}{\numberline {6}Topological helical states in a tight-binding model}{93}{chapter.6}%
\contentsline {section}{\numberline {6.1}The honeycomb lattice with a Kekulé-like hopping texture}{95}{section.6.1}%
\contentsline {subsection}{\numberline {6.1.1}Description of the model}{95}{subsection.6.1.1}%
\contentsline {subsection}{\numberline {6.1.2}Band diagram and width of the gap}{96}{subsection.6.1.2}%
\contentsline {section}{\numberline {6.2}Topological properties of the band diagram}{96}{section.6.2}%
\contentsline {subsection}{\numberline {6.2.1}Supressing inter-cluster coupling}{96}{subsection.6.2.1}%
\contentsline {subsection}{\numberline {6.2.2}Inversion of bands}{98}{subsection.6.2.2}%
\contentsline {subsection}{\numberline {6.2.3}Studying edge states on a nanoribbon}{98}{subsection.6.2.3}%
\contentsline {section}{\numberline {6.3}Topological invariants}{99}{section.6.3}%
\contentsline {subsection}{\numberline {6.3.1}The spin Chern number}{99}{subsection.6.3.1}%
\contentsline {subsection}{\numberline {6.3.2}The spin Bott index}{101}{subsection.6.3.2}%
\contentsline {subsection}{\numberline {6.3.3}Introducing a second control parameter}{102}{subsection.6.3.3}%
\contentsline {section}{\numberline {6.4}Link with experiments}{105}{section.6.4}%
\contentsline {chapter}{\numberline {7}Light in a deformed honeycomb atomic lattice}{109}{chapter.7}%
\contentsline {section}{\numberline {7.1}Angular-momentum eigenstates in a system of six atoms}{111}{section.7.1}%
\contentsline {section}{\numberline {7.2}Establishing the band diagram}{113}{section.7.2}%
\contentsline {subsection}{\numberline {7.2.1}Fourier transform of the Hamiltonian}{113}{subsection.7.2.1}%
\contentsline {subsection}{\numberline {7.2.2}Projecting the bands on the \textit {spdf pol} basis}{114}{subsection.7.2.2}%
\contentsline {section}{\numberline {7.3}Topological behaviour}{116}{section.7.3}%
\contentsline {subsection}{\numberline {7.3.1}Spin Chern number for light in a deformed honeycomb lattice}{116}{subsection.7.3.1}%
\contentsline {subsection}{\numberline {7.3.2}Influence of atomic spacing on gap properties}{116}{subsection.7.3.2}%
\contentsline {chapter}{\numberline {8}Conclusion and perspectives}{119}{chapter.8}%
\contentsline {chapter}{\numberline {9}Appendices}{123}{chapter.9}%
\contentsline {section}{\numberline {A}Vector bundles and fiber bundles}{123}{section.9.1}%
\contentsline {section}{\numberline {B}Derivation of the formula for the width of the spectral gap}{124}{section.9.2}%
